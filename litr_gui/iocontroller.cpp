/************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/

#include "iocontroller.h"
#include <QDebug>
#include "dialog.h"

IOController::IOController(QObject *parent) : QObject(parent)
{
    connect(this,&QObject::destroyed,&m_ghci,&QProcess::kill);
    m_ghci.setProcessChannelMode(QProcess::MergedChannels);
    Dialog diag;
    diag.exec();
    m_ghci.start(diag.getGhci(),{diag.getHs()});
    m_ghci.write("lambdaInterpreterI False (lrno True) lpNO\n");
    connect(&m_ghci,&QProcess::readyReadStandardOutput,[this](){
        while (m_ghci.canReadLine()){
            QString in = m_ghci.readLine();
            qInfo() << in;
            if (in == "clear\n") setText("");
            else if (in.startsWith("result: ")) setText(text().append(in.mid(8).replace('\n',"<br>")));
        }
    });
    m_timer.setSingleShot(true);
    connect(&m_timer,&QTimer::timeout,[this](){emit textChanged();});
}

IOController::~IOController()
{
    m_ghci.kill();
    m_ghci.waitForFinished();
}


QString IOController::text() const
{
    return m_text;
}

void IOController::setText(const QString &str)
{
    m_text = str;
    if (!m_timer.isActive()){
        emit textChanged();
        m_timer.start(0.05);
    }
}

void IOController::sendCmd(const QString &cmd)
{
    if (cmd == ")stop"){
        m_ghci.terminate();
        m_ghci.write("\nlambdaInterpreterI False (lrno True) lpNO\n");
    }
    else m_ghci.write(cmd.toLocal8Bit().append('\n'));
}
