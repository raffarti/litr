import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.1
import litr_gui 1.0 as Ctrl

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Lambda Interpreter")

    /*menuBar: MenuBar {
        Menu {
            title: qsTr("File")
            MenuItem {
                text: qsTr("&Open")
                onTriggered: console.log("Open action triggered");
            }
            MenuItem {
                text: qsTr("Exit")
                onTriggered: Qt.quit();
            }
        }
    }*/

    ColumnLayout{
        anchors.fill: parent
        Item {
            Layout.fillWidth: true
            height: input.height
            RowLayout{
                anchors.left: parent.left
                anchors.right: parent.right
                ComboBox {
                    Layout.fillWidth: true
                    model: ListModel {
                        id: history
                    }
                    focus: true
                    editable: true
                    onAccepted: {
                        Ctrl.IOController.sendCmd(currentText)
                        if (find(currentText) === -1) history.append({"text": currentText})
                    }
                    id: input
                }
                Button {
                    id: stop
                    iconName: "process-stop"
                    onClicked: Ctrl.IOController.sendCmd(")stop")
                }
                ComboBox{
                    model: ["Normal Order + Sharing", "Normal Order w/o Sharing", "Applicative Order"]
                    onCurrentIndexChanged: {
                        switch (currentIndex){
                        case 0:
                            Ctrl.IOController.sendCmd(":nosh");
                            break;
                        case 1:
                            Ctrl.IOController.sendCmd(":no");
                            break;
                        case 2:
                            Ctrl.IOController.sendCmd(":ao");
                            break;
                        }

                    }
                }
            }
        }
        ScrollView{
             id: flick

             Layout.fillWidth: true
             Layout.fillHeight: true
             flickableItem.contentWidth: edit.paintedWidth
             flickableItem.contentHeight: edit.paintedHeight
             clip: true


             TextEdit {
                 id: edit
                 Layout.fillWidth: true
                 width: flick.flickableItem.width
                 readOnly: true
                 wrapMode: TextEdit.Wrap
                 text: Ctrl.IOController.text
                 textFormat: TextEdit.RichText
             }
         }
    }
}

