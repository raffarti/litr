/************************************************************************
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
************************************************************************/
#include "dialog.h"
#include "ui_dialog.h"

#include <QFileDialog>

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
}

QString Dialog::getGhci() const
{
    return ui->ghciLine->text();
}

QString Dialog::getHs() const
{
    return ui->hsLine->text();
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::on_ghciButton_clicked()
{
    ui->ghciLine->setText(QFileDialog::getOpenFileName(this,"GHCi executable"));
}

void Dialog::on_hsButton_clicked()
{
    ui->hsLine->setText(QFileDialog::getOpenFileName(this,"Haskell source file"));
}
