TEMPLATE = app

QT += qml quickwidgets

SOURCES += main.cpp \
    iocontroller.cpp \
    dialog.cpp

RESOURCES += qml.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

win32{
    CONFIG += static
    DEFINES += STATIC
    LIBS += \
        libqwindows
}

HEADERS += \
    iocontroller.h \
    dialog.h

QMAKE_CXXFLAGS += -std=c++14

DISTFILES +=

FORMS += \
    dialog.ui
