import Data.Char
import Data.String
import Data.Maybe
import Data.List
import System.Info
import System.Process


-- SINTASSI
data LTer = LVar String | LAstr String LTer | LApp LTer LTer | LRef Int
    deriving Show
    
-- LEXER TOCKENS
data LToken = LLambda | LLPar | LRPar | LPoint | LVars String | LEnd | LError
    deriving (Show, Eq)
    
-- LEXER
str2ltkn :: String -> [LToken]
str2ltkn "" = [LEnd]
str2ltkn (x:xs) = case x of
                          '('     ->  LLPar : str2ltkn xs
                          ')'     ->  LRPar : str2ltkn xs
                          '.'     ->  LPoint : str2ltkn xs
                          '\\'    ->  LLambda : str2ltkn xs
                          z | isAlpha z ->  parseLVar "" (x:xs)
                            | isSpace z -> str2ltkn xs
                            | otherwise ->  [LError]
                        
    where
        parseLVar :: String -> String -> [LToken]
        parseLVar ss "" = [LVars ss,LEnd]
        parseLVar ss (x:xs) | isAlpha x = parseLVar (ss ++ [x]) xs
                            | otherwise = LVars ss : str2ltkn (x:xs)
        
{-
LambdaR -> \\LVars.LambdaR
LambdaR -> LApp
LApp    -> LApp LTer
LApp    -> LTer
LTer    -> (LambdaR)
LTer    -> v
LVars   -> v LVars
LVars   -> v
-}
-- PARSER MONOLITICO
parseLambda :: [LToken] -> Maybe LTer
parseLambda tks = case parseLambdaR tks of
                      Nothing -> Nothing
                      Just (t, [LEnd]) -> Just t
                      _ -> Nothing
    where
        readLApp :: LTer -> Maybe (LTer, [LToken]) -> Maybe (LTer, [LToken])
        readLApp _ Nothing = Nothing
        readLApp lter1 (Just (lter2, [LEnd])) = Just (LApp lter1 lter2, [LEnd])
        readLApp lter1 (Just (lter2, LRPar:tks)) = Just (LApp lter1 lter2, LRPar:tks)
        readLApp lter1 (Just (lter2, tks)) = readLApp (LApp lter1 lter2) (readLTer tks)
        
        readLVars :: [LToken] -> Maybe (LTer -> LTer, [LToken])
        readLVars (LVars v:LPoint:tks) = Just (\x -> LAstr v x,tks)
        readLVars (LVars v:v2:tks) = case readLVars (v2:tks) of
                                          Just (fter, tks) -> Just (\x -> LAstr v (fter x), tks)
                                          _ -> Nothing
        readLVars _ = Nothing
        
        
        readLTer :: [LToken] -> Maybe (LTer, [LToken])
        readLTer [LEnd] = Nothing
        readLTer (LLPar:tks) = case parseLambdaR tks of
                                    Just (lter, LRPar:tks) -> Just (lter, tks)
                                    _ -> Nothing
        readLTer (LVars v:tks) = Just (LVar v, tks)
        readLTer _ = Nothing
        
        parseLambdaR :: [LToken] -> Maybe (LTer, [LToken])
        parseLambdaR [LEnd] = Nothing
        parseLambdaR (LLambda:tks) = case readLVars tks of
                                         Just (_, [LEnd]) -> Nothing
                                         Just (fter, rtks) -> case parseLambdaR rtks of
                                                                   Just (ter, rrtks) -> Just (fter ter, rrtks)
                                                                   _ -> Nothing
                                         _ -> Nothing
        parseLambdaR tks = case readLTer tks of
                               Just (t, [LEnd]) -> Just (t, [LEnd])
                               Just (t, LRPar:tks) -> Just (t,LRPar:tks)
                               Just (lt, rtks) -> readLApp lt $ readLTer rtks
                               _ -> Nothing

-- PARSER POLIMORFO
type Parse a b = [a] -> [(b,[a])]

parseLambda2 :: [LToken] -> Maybe LTer
parseLambda2 tks = case (build (pLLambda >*> token LEnd) fst) tks of
                       [(a,[])] -> Just a
                       _ -> Nothing
    where
        
        succeed :: b -> Parse a b
        succeed val inp = [(val,inp)]
        
        none :: Parse a b
        none inp = []
        
        alt :: Parse a b -> Parse a b -> Parse a b
        alt p1 p2 inp = p1 inp ++ p2 inp
        
        token :: Eq a => a -> Parse a a
        token t = spot (==t)
        
        spot :: (a -> Bool) -> Parse a a
        spot p (x:xs) | p x = [(x,xs)]
                      | otherwise = []
        spot p [] = []
        
        token1 :: Eq a => a -> Parse a a
        token1 t = spot1 (==t)
        
        spot1 :: (a -> Bool) -> Parse a a
        spot1 p [] = []
        spot1 p (x:xs) | p x = [(x,x:xs)]
                       | otherwise = []
        
        isLVars :: LToken -> Bool
        isLVars (LVars _) = True
        isLVars _ = False
        
        (>*>) :: Parse a b -> Parse a c -> Parse a (b,c)
        (>*>) p1 p2 inp = [((y,z),rem2) | (y, rem1) <- p1 inp, (z,rem2) <- p2 rem1]
        
        list :: Parse a b -> Parse a [b]
        list p = (succeed []) `alt` ( (p >*> list p) `build` (uncurry (:)) )
        
        list1 :: Parse a b -> Parse a [b]
        list1 p tks = filter (\x -> case x of {([],_) -> False; _ -> True}) (list p tks)
                                         
        build :: Parse a b -> (b -> c) -> Parse a c
        build p f inp = [(f x,rem) | (x,rem) <- p inp]
        
        pLVars :: Parse LToken [LToken]
        pLVars = list1 (spot isLVars)
                                     
        pLTer :: Parse LToken LTer
        pLTer = pLPars `alt` build (spot isLVars) (\(LVars x) -> LVar x)
        
        pLPars :: Parse LToken LTer
        pLPars = build (token LLPar >*> pLLambda >*> token LRPar) (\((_,t),_) -> t)
        
        pLApp :: Parse LToken LTer
        pLApp = build (list1 pLTer >*> alt (token1 LRPar) (token1 LEnd)) (\(x,y) -> foldl1 (\ac y -> LApp ac y) x)
        
        pLLambda :: Parse LToken LTer
        pLLambda = build (pLApp `alt` build (token LLambda >*> pLVars >*> token LPoint >*> pLLambda >*> alt (token1 LRPar) (token1 LEnd)) (\((((_,vs),_),t),_) -> foldr (\(LVars x) ac -> LAstr x ac) t vs) >*> alt (token1 LRPar) (token1 LEnd)) fst

--

-- POINTER TABLE
type LRefTable = [(Int, LTer)]
-- Nella maggior parte dei casi un LTer deve essere accompagnato da una tabella per le referenze
type LExpr = (LRefTable, LTer)

-- Funzioni di manipolazione della tabella delle referenze
getLRef :: LRefTable -> Int -> LTer
getLRef tbl x = snd $ fromJust $ find ((==x).fst) tbl

addLRef :: LRefTable -> LTer -> LExpr
addLRef = addLRefR 0
    where
        addLRefR :: Int -> LRefTable -> LTer -> LExpr 
        addLRefR n tbl lter | any ((==n).fst) tbl = addLRefR (n+1) tbl lter
                            | otherwise = ((n, lter):tbl, LRef n)
                        
showLSub :: LRefTable -> LTer -> LTer -> String -> String
showLSub tbl t0 t1 x = '(':showLExpr lpFV (paint "red",x) (tbl,t0) ++ ")["++ paint "green" (showSLExpr (tbl,t1)) ++ "/" ++ paint "red" x ++ "]"
    where
        paint :: String -> String -> String
        paint c x = "<span style=\"color:" ++ c ++ "\">" ++ x ++ "</span>"

uptLRef :: LRefTable -> Int -> LTer -> LRefTable
uptLRef tbl x t = (x,t):filter ((/=x).fst) tbl



-- FUNZIONI DI STAMPA

-- Tipo per le funzioni ausiliarie di stampa ("Decoratori")
-- Queste funzioni possono fare uso di una "memoria" di tipo a
-- Un decoratore prende in input un'espressione e
-- restituisce una lista di coppie (Memoria, Funzione di "decorazione")]
-- Ogni coppia del risultato corrizponde ad un parametro del costruttore dell'espressione
type LPrinter a = a -> LExpr -> [(a,String -> String)]

-- Funzione di concatenazione di Decoratori
(>+>) :: LPrinter a -> LPrinter b -> LPrinter (a,b)
(>+>) p0 p1 (m0,m1) le = zipWith (\(m0,s0) (m1,s1) -> ((m0,m1), s1.s0)) (p0 m0 le) (p1 m1 le)
        
-- Decoratore di variabili libere
lpFV :: LPrinter (String->String,String)
lpFV (f,"") _ = repeat ((f,""),id)
lpFV (f,s) (_, LAstr x t) | s == x = [((f,""),id),((f,""),id)]
                          | otherwise = [((f,""),id),((f,s),id)]
lpFV (f,s) (_, LVar x) | x == s = [((f,""),f)]
                       | otherwise = [((f,""),id)]
lpFV (f,s) (_,_) = repeat ((f,s),id)

-- Funzione di stampa "base"
showSLExpr :: LExpr -> String
showSLExpr = showLExpr (\_ _ -> repeat ((),id)) ()

-- Funzione principale di stampa
-- Include i decoratori essenziali (Parentesi, simboli per l'astrazione, evidenziazione referenze
-- visita l'albero decorandone nodi e foglie
showLExpr :: LPrinter a -> a -> LExpr -> String
showLExpr ps m = showLExprR (ps >+> lpAstr >+> lpRef >+> lpPars) (((m,False),()),-1)
    where
        showLExprR :: LPrinter a -> a -> LExpr -> String
        showLExprR ps m expr@(_,LVar x) = (snd.head) (ps m expr) $ x
        showLExprR ps m expr@(tbl, LAstr x t) = (\[(_,s0),(m1,s1)] -> s0 x ++ s1 (showLExprR ps m1 (tbl,t))) $ take 2 $ ps m expr
        showLExprR ps m expr@(tbl, LApp t0 t1) = (\[(m0,s0),(m1,s1)] -> s0 (showLExprR ps m0 (tbl,t0)) ++ " " ++ s1 (showLExprR ps m1 (tbl,t1))) $ take 2 $ ps m expr
        showLExprR ps m expr@(tbl, LRef x) = (\(m,s) -> s $ showLExprR ps m (tbl,getLRef tbl x)) $ head $ ps m expr
        
        lpPars :: LPrinter Int
        lpPars v (_, LVar _) = [(0,id)]
        lpPars v (_, LAstr _ _) | v > 1 = [(1,\x -> '(':x),(1,\x -> x ++ ")")]
                                | otherwise = [(1,id),(1,id)]
        lpPars v (_, LApp _ _) | v > 2 = [(2,\x -> '(':x),(3,\x -> x ++ ")")]
                               | otherwise = [(2,id),(3,id)]
        lpPars v (_, LRef _) = [(v,id)]
        
        lpAstr :: LPrinter Bool
        lpAstr c (tbl, LAstr _ t) = [ (True,\x -> (if c then ' ' else '\\'):x ++ if lisAstr (tbl,t) then "" else ".") , (True,id) ]
        lpAstr s (_, LRef _) = [(s,id),(s,id)]
        lpAstr _ _ = repeat (False,id)

        lpRef :: LPrinter ()
        lpRef _ (_, LRef _) = [((),\x->"<u>"++x++"</u>")]
        lpRef _ (_, _) = repeat ((), id)
        

-- Restituisce la lista delle variabili libere in un'espressione
lfv :: LExpr -> [String]
lfv (tbl,(LVar x)) = [x]
lfv (tbl,(LAstr x t)) = filter (/=x) $ lfv (tbl,t)
lfv (tbl,(LApp t1 t2)) = lfv (tbl,t1) ++ lfv (tbl,t2)
lfv (tbl,(LRef x)) = lfv (tbl,getLRef tbl x)
              
-- Determina se una variabile è libera all'interno di tutti i termini di una lista
lisVF :: LRefTable -> String -> [LTer] -> Bool
lisVF tbl x y = elem x (concat $ map (curry lfv tbl) y)

-- Determina se un termine è un'astrazione (trasparente alle referenze)
lisAstr :: LExpr -> Bool
lisAstr expr = case lderef expr of
                    (LAstr _ _) -> True
                    _ -> False

-- Restituisce il termine privo di referenze alla radice
lderef :: LExpr -> LTer
lderef (tbl,(LRef x)) = curry lderef tbl $ getLRef tbl x
lderef (_,t) = t


-- Verifica se un termine è un valore
lisV :: LExpr -> Bool
lisV (tbl,(LRef x)) = lisV (tbl, getLRef tbl x)
lisV (tbl,(LVar _)) = True
lisV (tbl,(LAstr _ _)) = True
lisV (tbl,(LApp t0 _)) = False
                       
-- Verifica se un termine è in forma normale (irriducibile in Outermost)
lisNF :: LExpr -> Bool
lisNF (tbl,(LRef x)) = lisNF (tbl, getLRef tbl x)
lisNF (tbl,(LVar _)) = True
lisNF (tbl,(LAstr _ _)) = True
lisNF (tbl,(LApp t0 _)) | lisNF (tbl,t0) = False
                        | otherwise = lisNF (tbl,t0)

-- Restituisce l'espressione risultante dalla sostituzione, nel primo termine, della variabile col secondo termine
lsub :: LRefTable -> LTer -> LTer -> String -> LExpr
lsub tbl (LVar x) t0 y | x == y = (tbl, t0)
                       |otherwise = (tbl, LVar x)
lsub tbl (LAstr x t) t0 y | x == y = (tbl, LAstr x t)
                          | lisVF tbl x [t0] = (\(rtbl,rt) -> lsub rtbl (LAstr s rt) t0 y) $ lsub tbl t (LVar s) x
                          | otherwise = (\(rtbl,rt) -> (rtbl, LAstr x rt)) $ lsub tbl t t0 y
    where
        -- funzione per la generazione di nomi di variabili
        ec xs ys = (\x -> x ++ ec xs x) $ concat [[z:zs] | z <- xs, zs <- ys]
        -- variabile non libera nei termini dell'astrazione
        s = head $ dropWhile (flip (lisVF tbl) [t,t0]) $ ec ['a'..'z'] [""]
lsub tbl (LApp t1 t2) t0 y = (\(rtbl,rt1) -> (\(rrtbl,rt2) -> (rrtbl,LApp rt1 rt2)) $ lsub rtbl t2 t0 y) $ lsub tbl t1 t0 y
lsub tbl (LRef x) t0 y = (\(rtbl, rt) -> (uptLRef rtbl x rt, LRef x)) $ lsub tbl (getLRef tbl x) t0 y


-- Passo di riduzione "by name" (Outermost, Normal Order)
-- Se il primo argomento è True, potrà generare referenze
lrno :: Bool -> LExpr -> (LExpr, String)
lrno mr (tbl, (LRef x)) = case lrno mr (tbl,getLRef tbl x) of
                              ((rtbl,t),m) | lisAstr (rtbl,t) -> ((uptLRef rtbl x t,lderef (rtbl,t)),m)
                                           | otherwise -> ((uptLRef rtbl x t,LRef x),m)
lrno mr (tbl,(LApp (LAstr x t0) t1)) | lisV (tbl,t1) || not mr = (lsub tbl t0 t1 x, showLSub tbl t0 t1 x)
                                     | otherwise = (\(tbl,ref) -> (lsub tbl t0 ref x, showLSub tbl t0 t1 x)) $ addLRef tbl t1
lrno mr (tbl,(LApp t0 t1)) | lisAstr (tbl,t0) = lrno mr (tbl,LApp (lderef (tbl,t0)) t1)
                           | lisNF (tbl,t0) = ((tbl,LApp t0 t1), "Irreducible")
                           | otherwise = (\((tbl,t),m) -> ((tbl,LApp t t1) ,m)) $ lrno mr (tbl,t0)
lrno mr (tbl,x) = ((tbl, x), "Irreducible")

-- Passo di riduzione "by value" (Outermost, Applicative Order)
lrao :: LExpr -> (LExpr, String)
lrao (tbl,LVar x) = ((tbl,LVar x), "Irreducible")
lrao (tbl,LApp (LAstr x t0) t1) | lisV (tbl,t1) = (lsub tbl t0 t1 x, showLSub tbl t0 t1 x)
                                | otherwise = (\((tbl,t),m) -> ((tbl,LApp (LAstr x t0) t),m)) $ lrao (tbl,t1)
lrao (tbl,LApp t0 t1) | (not.lisNF) (tbl,t0) = (\((tbl,t),m) -> ((tbl,LApp t t1),m)) $ lrao (tbl,t0)
                      | otherwise = ((tbl, LApp t0 t1), "Irreducible")
lrao (tbl,LAstr x t0) = ((tbl,LAstr x t0), "Irreducible")

-- Decoratore di astrazioni riducibili da lrno
lpNO :: LPrinter Bool
lpNO False _ = repeat (False,id)
lpNO _ (tbl, LApp t0 t1) | lisAstr (tbl,t0) = [(False,hl0),(False,hl1)]
                         | lisNF (tbl,t0) = [(False,id),(False,id)]
                         | otherwise = [(True,id),(False,id)]
    where
        hl1 x = "<span style='background-color:lightgreen'>" ++ x ++ "</span>"
        hl0 x = "<span style='background-color:red'>" ++ x ++ "</span>"
lpNO b (tbl, LRef x) = [(b,id)]
lpNO _ _ = repeat (False,id)

-- Decoratore di astrazioni riducibili da lrao
lpAO :: LPrinter Bool
lpAO False _ = repeat (False,id)
lpAO _ (tbl, LApp t0 t1) | lisAstr (tbl,t0) && lisV (tbl,t1) = [(False,hl0),(False,hl1)]
                         | lisAstr (tbl,t0) = [(False,id),(True,id)]
                         | lisNF (tbl,t0) = [(False,id),(False,id)]
                         | otherwise = [(True,id),(False,id)]
    where
        hl0 x = "<span style='background-color:lightgreen'>" ++ x ++ "</span>"
        hl1 x = "<span style='background-color:red'>" ++ x ++ "</span>"

lpAO b (tbl, LRef x) = [(b,id)]
lpAO _ _ = repeat (False,id)

-- Funzione interprete
lambdaInterpreter :: IO()
lambdaInterpreter = lambdaInterpreterI True (lrno True) lpNO

lambdaInterpreterI :: Bool -> (LExpr -> (LExpr,String)) -> LPrinter Bool -> IO ()
lambdaInterpreterI toFile lredr lpred = do input <- getLine
                                           if not toFile then putStrLn "\nclear" else return ()
                                           case input of
                                                ":no" -> do putStrLn $ ret "Riduzione \"call by name\""
                                                            lambdaInterpreterI toFile (lrno False) lpNO
                                                ":nosh" -> do putStrLn $ ret "Riduzione \"call by name\" con condivisione"
                                                              lambdaInterpreterI toFile (lrno True) lpNO
                                                ":ao" -> do putStrLn $ ret "Riduzione \"call by value\""
                                                            lambdaInterpreterI toFile lrao lpAO
                                                _ -> do (if toFile then writeFile "tmp.html" else putStrLn) $ ret (
                                                           case parseLambda2 $ str2ltkn input of
                                                                 (Just t) -> "<table>" ++ lProcess ([], t) ++ "</table>"
                                                                 Nothing -> "<span style='color:red'> &lt;Parsing Error&gt; </span>")
                                                        if toFile then case os of
                                                                            "linux" -> callCommand "xdg-open tmp.html"
                                                                            "windows" -> callCommand "start tmp.html" --untested
                                                                            _ -> return ()
                                                                  else lambdaInterpreterI toFile lredr lpred

    where
        ret :: String -> String
        ret = if not toFile then ("result: " ++)
                            else  id
        
        lProcess :: LExpr -> String
        lProcess e = case lredr e of
                               (re,"Irreducible") -> td (showSLExpr re) ""
                               (re,m) -> td (showLExpr lpred (True) e) m ++ "\n" ++ (ret $ lProcess re)
            where
                td x y = "<tr><td>" ++ x ++ "</td><td style='padding-left:20px'>" ++ y ++ "</td></tr>"