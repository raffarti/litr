# Interprete Lambda Calcolo Puro


## da GHCi

Utilizzare la funzione 
    
    lambdaInterpreter
    
per avviare l'interprete. L'output sarà salvato nel file tmp.html nella
cartella di lavoro, tale file dovrebbe eseere automaticamente aperto nel
browser predefinito.


## da interfaccia grafica

L'interfaccia richiede le librerie di **Qt5**

_Qt4 non è sufficiente!_

### Compilazione

Per semplicità utilizzare Qt Creator per la compilazione. 



#### Window

Scaricando le librerie di sviluppo di Qt5, Qt Creator è incluso

http://www.qt.io/download-open-source/

#### Linux

I pacchetti di Qt5 sono disponibili sulla stragrande maggioranza delle
distribuzioni Linux, altrimenti sono anch'essi disponibili sul sito ufficiale

http://www.qt.io/download-open-source/

#### pacchetti requisiti

* qt5-qtbase-devel
* qt5-qtquickcontrols

_nota: il nome dei pacchetti varia leggermente a seconda della distribuzione_